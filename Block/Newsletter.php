<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Block;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Professio\Inboxify\Model\Client;
use Professio\Inboxify\Helper\Config;

/**
 * Newsletter management block
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class Newsletter extends Template
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * Constructor
     * @param Client $client
     * @param Config $configHelper
     * @param Context $context
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Client $client,
        Config $configHelper,
        Context $context,
        Session $customerSession,
        array $data = []
    ) {
    
        parent::__construct($context, $data);
        
        $this->client = $client;
        $this->configHelper = $configHelper;
        $this->customerSession = $customerSession;
        
        $this->setTemplate('inboxify/newsletter.phtml');
    }

    /**
     * Get URL for the form
     * @return string
     */
    public function getAction()
    {
        return $this->getUrl('*/*/save');
    }
    
    /**
     * Get config helper
     * @return Config
     */
    public function getConfigHelper()
    {
        return $this->configHelper;
    }
    
    /**
     * Get current customer
     * @return Customer
     */
    protected function getCustomer()
    {
        return $this->customerSession->getCustomer();
    }
    
    /**
     * Check if current contact is subscribed
     * @return boolean
     */
    public function isSubscribed()
    {
        if (!isset($this->isSubscribed)) {
            $contact = $this->client->getClient()
                ->getContact(
                    $this->getCustomer()->getEmail(),
                    $this->getConfigHelper()->getGeneralList()
                );
            
            $this->isSubscribed = $contact ? !$contact->unsubscribed : false;
        }
        
        return $this->isSubscribed;
    }
}
