<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Block\Adminhtml\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Data\Form;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Ui\Component\Layout\Tabs\TabInterface;
use Professio\Inboxify\Model\Client;

/**
 * Customer account form block
 */
class Newsletter extends \Magento\Backend\Block\Widget\Form\Generic implements TabInterface
{
    /**
     * @var string
     */
    protected $_template = 'Professio_Inboxify::tab/newsletter.phtml';

    /**
     * @var Client
     */
    protected $client;
    
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    
    /**
     * Constructor
     * @param AccountManagementInterface $customerAccountManagement
     * @param Client $client
     * @param Context $context
     * @param CustomerRepositoryInterface $customerRepository
     * @param FormFactory $formFactory
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        AccountManagementInterface $customerAccountManagement,
        Client $client,
        Context $context,
        CustomerRepositoryInterface $customerRepository,
        FormFactory $formFactory,
        Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        
        $this->client = $client;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Return Tab label
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('Inboxify');
    }

    /**
     * Return Tab title
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('Inboxify');
    }

    /**
     * Tab class getter
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * Return URL link to Tab content
     * @return string
     */
    public function getTabUrl()
    {
        return '';
    }

    /**
     * Tab should be loaded trough Ajax call
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }

    /**
     * Can show tab in tabs
     * @return boolean
     */
    public function canShowTab()
    {
        return $this->_coreRegistry->registry(
            RegistryConstants::CURRENT_CUSTOMER_ID
        );
    }

    /**
     * Tab is hidden
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Initialize the form.
     * @return Newsletter
     */
    public function initForm()
    {
        if (!$this->canShowTab()) {
            return $this;
        }
        
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('_newsletter');
        $customerId = $this->_coreRegistry->registry(
            RegistryConstants::CURRENT_CUSTOMER_ID
        );

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Newsletter Information')]
        );

        $fieldset->addField(
            'inboxify_subscribe',
            'checkbox',
            [
                'label' => __('Subscribed to Newsletter'),
                'name' => 'inboxify_subscribe',
                'data-form-part' => $this->getData('target_form'),
                'onchange' => 'this.value = this.checked;'
            ]
        );

        if ($this->customerAccountManagement->isReadOnly($customerId)) {
            $form->getElement('inboxify_subscribe')
                ->setReadonly(true, true);
        }
        
        $isSubscribed = $this->isSubscribed($customerId);
        
        $form->setValues(
            ['inboxify_subscribe' => $isSubscribed ? 'true' : 'false']
        );
        $form->getElement('inboxify_subscribe')
            ->setIsChecked($isSubscribed);

        $this->updateFromSession($form, $customerId);
        $this->setForm($form);
        
        return $this;
    }
    
    /**
     * Check if customer is subscribed by customer id
     * @param integer $customerId
     * @return boolean
     */
    protected function isSubscribed($customerId)
    {
        $customer = $this->customerRepository->getById($customerId);
        $client = $this->client
            ->getStoreClient($customer->getStoreId());
        $contact = $client->getContact(
            $customer->getEmail()
        );
        
        return $contact && !$contact->unsubscribed;
    }

    /**
     * Update form elements from session data
     *
     * @param Form $form
     * @param int $customerId
     * @return void
     */
    protected function updateFromSession(Form $form, $customerId)
    {
        $data = $this->_backendSession->getCustomerFormData();
        
        if (!empty($data)) {
            $dataCustomerId = isset($data['customer']['entity_id']) ? $data['customer']['entity_id'] : null;
            
            if (isset($data['inboxify_subscribe']) && $dataCustomerId == $customerId) {
                $form->getElement('inboxify_subscribe')->setIsChecked($data['inboxify_subscribe']);
            }
        }
    }

    /**
     * Prepare html output
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->canShowTab()) {
            $this->initForm();
            return parent::_toHtml();
        } else {
            return '';
        }
    }
}
