<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\UrlInterface;

/**
 * Export page content block
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class Export extends Template
{
    /**
     * Get URL for export customers
     * @return mixed
     */
    public function getExportCustomersUrl()
    {
        return $this->_urlBuilder->getUrl('inboxify/export/customers');
    }
    
    /**
     * Get URL for export unregistered customers
     * @return mixed
     */
    public function getExportUnregisteredUrl()
    {
        return $this->_urlBuilder->getUrl('inboxify/export/unregistered');
    }
    
    /**
     * Get URL for export subscribers
     * @return mixed
     */
    public function getExportSubscribersUrl()
    {
        return $this->_urlBuilder->getUrl('inboxify/export/subscribers');
    }
}
