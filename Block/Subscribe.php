<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Professio\Inboxify\Helper\Config;

/**
 * Newsletter subscription form block
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class Subscribe extends Template
{

    /**
     * @var \Professio\Inboxify\Helper\Config
     */
    protected $configHelper;

    public function __construct(
        Config $configHelper,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        
        $this->configHelper = $configHelper;
    }

    /**
     * Retrieve form action url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('inboxify/subscriber/new', ['_secure' => true]);
    }
    
    /**
     * Get config helper
     * @return Config
     */
    public function getConfigHelper()
    {
        return $this->configHelper;
    }
}
