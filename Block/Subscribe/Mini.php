<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Block\Subscribe;

use Magento\Framework\DataObjectFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Professio\Inboxify\Helper\Config;
use Professio\Inboxify\Model\Config\Source\Account;

/**
 * Mini subscribe widget (checkbox)
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class Mini extends Template
{
    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * Constructor
     * @param Config $configHelper
     * @param Context $context
     * @param DataObjectFactory $dataObjectFactory
     * @param array $data
     */
    public function __construct(
        Config $configHelper,
        Context $context,
        DataObjectFactory $dataObjectFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        
        $this->configHelper = $configHelper;
        $this->dataObjectFactory = $dataObjectFactory;
    }

    /**
     * Get config helper
     * @return Config
     */
    public function getConfigHelper()
    {
        return $this->configHelper;
    }
    
    /**
     * Get form data, in fact only returns new varien object
     * @return \Magento\Framework\DataObject
     */
    public function getFormData()
    {
        return $this->dataObjectFactory->create();
    }
    
    /**
     * Check if sign-up is hidden
     * @return bool
     */
    public function isSignupHidden()
    {
        return Account::HIDDENCHECKED == $this->configHelper
            ->getAdvancedCreateAccount();
    }
    
    /**
     * Check if sign-up is checked
     * @return bool
     */
    public function isSignupChecked()
    {
        $v = $this->configHelper->getAdvancedCreateAccount();
        
        return Account::HIDDENCHECKED == $v || Account::CHECKED == $v;
    }
}
