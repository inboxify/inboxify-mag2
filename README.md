# Professio Inboxify Integration for Magento 2

This repository contains official Inboxify client for Magento 2.

## Requirements

- PHP >= 5.3
- [PHP module HASH](http://php.net/manual/en/book.hash.php)
- [PHP module JSON](http://php.net/manual/en/book.json.php)
- [PHP module Sockets](http://php.net/manual/en/book.sockets.php)
- [BudgetMailer API Account](https://www.budgetmailer.nl/aanmelden.php)
- Magento version 1.6 or higher

## Installation

You can install module using one of the following methods.

### Magento Marketplace

XXX The module is being currently in approval process for Magento Marketplace.
After it's approved we will provide you with the link here.

### Composer

You can install module using composer package management tool:

- download the module and API client: `composer require professio/magento2`
- install module to Magento 2: `php bin/magento setup:upgrade`

## Configuration

After module installation go to Magento Configuration back-end, then in menu
select `Store` -> `Configuration`, and click on `Inboxify` in `Customers` tab.

The required configuration directives are: `API Endpoint`, `API Key`, `API Secret` and `List`.

After initial API configuration is saved, the list of mailing lists will 
be loaded automatically.

## Copyright

MIT License

## Contact Information

- Email: [info@inboxify.nl](mailto:info@inboxify.nl)
- Website: [Inboxify](https://www.inboxify.nl/index.php)

## Changelog

- 1.0.2 (2017-06-30):
    - version bump for composer

- 1.0.1 (2017-06-30):
    - change API endpoint and other URLs from .eu to .nl

- 1.0.0:
    - Initial version
