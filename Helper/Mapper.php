<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Helper;

use Magento\Customer\Model\Address\AbstractAddress;
use Magento\Customer\Model\Customer;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Newsletter\Model\Subscriber;
use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order;
use Professio\Inboxify\Helper\Config;
use \Professio\Inboxify\Model\Config\Source\Address as ConfigSourceAddress;
use Psr\Log\LoggerInterface;

/**
 * Helper mapper
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class Mapper extends AbstractHelper
{
    /**
     * Address model to contact model
     * @var array
     */
    protected $addressToContact = array(
        'company' => 'companyName',
        // INFO for now not replacing multiple to single line
        'street' => 'address',
        'postcode' => 'postalCode',
        'city' => 'city',
        'country_id' => 'countryCode',
        'telephone' => 'telephone',
    );
    
    /**
     * List of countries by id
     * @var array
     */
    protected $countriesIdIso = array();
    
    /**
     * List of countries by ISO3 code
     * @var array
     */
    protected $countriesIsoId = array();
    
    /**
     * Customer model to API object
     * @var array
     */
    protected $customerToContact = array(
        'email' => 'email',
        'firstname' => 'firstName',
        'middlename' => 'insertion',
        'lastname' => 'lastName',
        'sex' => 'gender',
    );
    
    /**
     * Map list ids to names
     * @var array
     */
    protected $listIdToName;
    
    /**
     * Map list names to ids
     * @var array
     */
    protected $listNameToId;
    
    /**
     * Order to model
     * @var array
     */
    protected $orderToContact = array(
        'customer_email' => 'email',
        'customer_firstname' => 'firstName',
        'customer_middlename' => 'insertion',
        'customer_lastname' => 'lastName',
    );

    /**
     * @var CountryFactory
     */
    protected $directoryCountryFactory;

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        Context $context,
        CountryFactory $directoryCountryFactory,
        Config $configHelper
    ) {
        parent::__construct($context);
        
        $this->configHelper = $configHelper;
        $this->directoryCountryFactory = $directoryCountryFactory;
        $this->logger = $context->getLogger();
    }

    
    /**
     * Map address to API contact object
     *
     * @param AbstractAddress|Address $address
     * @param \stdClass $contactApi
     */
    public function addressToContact(
        $address,
        $contactApi
    ) {
    
        if (!($address instanceof AbstractAddress)
            && !($address instanceof Address)
        ) {
            throw new \InvalidArgumentException(__('Wrong address type.'));
        }
        
        if (!is_object($contactApi)) {
            $contactApi = new \stdClass();
        }
        
        foreach ($this->addressToContact as $keyModel => $keyApi) {
            $contactApi->{$keyApi} = $address->getData($keyModel);
        }
        
        $contactApi->countryCode =
            $this->countryIdToCountryCode($contactApi->countryCode);
        
        return $contactApi;
    }
    
    /**
     * Map country id to country code
     *
     * @param string $countryId
     *
     * @return string
     */
    public function countryIdToCountryCode($countryId)
    {
        if (!isset($this->countriesIdIso[$countryId])) {
            $country = $this->directoryCountryFactory->create()->load($countryId);
            $this->countriesIdIso[$countryId] = $country->getData('iso3_code');
        }
        
        return $this->countriesIdIso[$countryId];
    }
    
    /**
     * Map customer model to api contact
     *
     * @param Customer $customer
     * @param \stdClass $contactApi
     */
    public function customerToContact(Customer $customer, $contactApi = null)
    {
        if (!is_object($contactApi)) {
            $contactApi = new \stdClass();
        }
        
        foreach ($this->customerToContact as $keyModel => $keyApi) {
            $contactApi->{$keyApi} = $customer->getData($keyModel);
        }
        
        if ($this->configHelper
            ->isAdvancedOnAddressUpdateEnabled()) {
            if (ConfigSourceAddress::BILLING
                == $this->configHelper->getAdvancedAddressType()
            ) {
                $address = $customer->getDefaultBillingAddress();
            } elseif (ConfigSourceAddress::SHIPPING
                == $this->configHelper->getAdvancedAddressType()
            ) {
                $address = $customer->getDefaultShippingAddress();
            } else {
                $address = false;
            }
            
            if ($address && $address->getId()) {
                $this->addressToContact($address, $contactApi);
            }
        }
        
        return $contactApi;
    }
    
    /**
     * Map order to contact
     * @param Order $order
     * @param \stdClass $contactApi
     * @return \stdClass
     */
    public function orderToContact(Order $order, $contactApi = null)
    {
        if (!is_object($contactApi)) {
            $contactApi = new \stdClass();
        }
        
        foreach ($this->orderToContact as $keyOrder => $keyApi) {
            $contactApi->{$keyApi} = $order->getData($keyOrder);
        }
        
        $this->logger->debug('fml: ' . json_encode($contactApi));
        
        if ($this->configHelper
            ->isAdvancedOnAddressUpdateEnabled()) {
            if (ConfigSourceAddress::BILLING
                == $this->configHelper->getAdvancedAddressType()
            ) {
                $address = $order->getBillingAddress();
            } elseif (ConfigSourceAddress::SHIPPING
                == $this->configHelper->getAdvancedAddressType()
            ) {
                $address = $order->getShippingAddress();
            } else {
                $address = false;
            }

            if ($address && $address->getId()) {
                $this->addressToContact($address, $contactApi);
            }
        }
        
        $this->logger->debug('fml2: ' . json_encode($contactApi));
        
        return $contactApi;
    }
    
    /**
     * Map newsletter subscriber to contact
     * @param Subscriber $subscriber
     * @param \stdClass $contactApi
     * @return \stdClass
     */
    public function subscriberToContact(Subscriber $subscriber, $contactApi = null)
    {
        if (is_null($contactApi)) {
            $contactApi = new \stdClass();
        }
        
        $contactApi->email = $subscriber->getSubscriberEmail();
        $contactApi->subscribe = ($subscriber->getSubscriberStatus() == 1);
        $contactApi->unsubscribed = !($subscriber->getSubscriberStatus() == 1);
        
        return $contactApi;
    }
}
