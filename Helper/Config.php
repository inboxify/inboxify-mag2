<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Professio\Inboxify\Model\Config\Source\Address;
use Professio\Inboxify\Model\Config\Source\Delete;
use Professio\Inboxify\Model\Config\Source\Update;

/**
 * Config helper
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class Config extends AbstractHelper
{
    const CONFIG_PATH_ADVANCED_ADDRESS_TYPE =
        'inboxify/advanced/address_type';
    const CONFIG_PATH_ADVANCED_ON_ADDRESS_UPDATE =
        'inboxify/advanced/on_address_update';
    const CONFIG_PATH_ADVANCED_ON_CUSTOMER_DELETE =
        'inboxify/advanced/on_customer_delete';
    const CONFIG_PATH_ADVANCED_ON_CUSTOMER_UPDATE =
        'inboxify/advanced/on_customer_update';
    const CONFIG_PATH_ADVANCED_ON_CREATE_ACCOUNT =
        'inboxify/advanced/on_create_account';
    const CONFIG_PATH_ADVANCED_ON_ORDER =
        'inboxify/advanced/on_order';
    const CONFIG_PATH_ADVANCED_FRONTEND =
        'inboxify/advanced/frontend';

    const CONFIG_PATH_API_ENDPOINT = 'inboxify/api/endpoint';
    const CONFIG_PATH_API_KEY = 'inboxify/api/key';
    const CONFIG_PATH_API_SECRET = 'inboxify/api/secret';

    const CONFIG_PATH_GENERAL_LIST = 'inboxify/general/list';

    const CONFIG_PATH_CACHE_ENABLED = 'inboxify/cache/enabled';
    const CONFIG_PATH_CACHE_TTL = 'inboxify/cache/ttl';

    /**
     * @var DirectoryList $directory_list
     */
    protected $directoryList;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Constructor
     * @param Context $context
     * @param DirectoryList $directoryList
     */
    public function __construct(
        Context $context,
        DirectoryList $directoryList,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);

        $this->directoryList = $directoryList;
        $this->scopeConfig = $context->getScopeConfig();
        $this->storeManager = $storeManager;
    }

    /**
     * Get address type
     *
     * @param int|null $storeId
     * @return string
     */
    public function getAdvancedAddressType($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_ADVANCED_ADDRESS_TYPE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get sign-up configuration while creating an account
     *
     * @param int|null $storeId
     * @return string
     */
    public function getAdvancedCreateAccount($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_ADVANCED_ON_CREATE_ACCOUNT,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get if selected address type is billing
     *
     * @param int|null $storeId
     * @return boolean
     */
    public function isAddressTypeBilling($storeId = null)
    {
        return $this->getAdvancedAddressType($storeId) == Address::BILLING;
    }

    /**
     * Get if selected address type is shipping
     *
     * @param int|null $storeId
     * @return boolean
     */
    public function isAddressTypeShipping($storeId = null)
    {
        return $this->getAdvancedAddressType($storeId) == Address::SHIPPING;
    }

    /**
     * Get enabled front-end
     *
     * @param int|null $storeId
     * @return boolean
     */
    public function isAdvancedFrontendEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_ADVANCED_FRONTEND,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get on address update enabled
     *
     * @param int|null $storeId
     * @return boolean
     */
    public function isAdvancedOnAddressUpdateEnabled($storeId = null)
    {
        $v = $this->scopeConfig->getValue(
            self::CONFIG_PATH_ADVANCED_ON_ADDRESS_UPDATE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return Update::ON_UPDATE_UPDATE == $v;
    }

    /**
     * Get on customer delete enabled
     *
     * @param int|null $storeId
     * @return boolean
     */
    public function isAdvancedOnCustomerDeleteEnabled($storeId = null)
    {
        $v = $this->scopeConfig->getValue(
            self::CONFIG_PATH_ADVANCED_ON_CUSTOMER_DELETE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return Delete::ON_DELETE_DELETE == $v;
    }

    /**
     * Get on customer delete and unsubscribe enabled
     *
     * @param int|null $storeId
     * @return boolean
     */
    public function isAdvancedOnCustomerDeleteUnsubscribeEnabled($storeId = null)
    {
        $v = $this->scopeConfig->getValue(
            self::CONFIG_PATH_ADVANCED_ON_CUSTOMER_DELETE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return Delete::ON_DELETE_DEL_UNSUB == $v;
    }

    /**
     * Get on customer update enabled
     *
     * @param int|null $storeId
     * @return boolean
     */
    public function isAdvancedOnCustomerUpdateEnabled($storeId = null)
    {
        $v = $this->scopeConfig->getValue(
            self::CONFIG_PATH_ADVANCED_ON_CUSTOMER_UPDATE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return Update::ON_UPDATE_UPDATE == $v;
    }

    /**
     * Get on new order
     *
     * @param int|null $storeId
     * @return boolean
     */
    public function isAdvancedOnOrderEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_ADVANCED_ON_ORDER,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get config for inboxify client.
     *
     * @param integer|null $storeId
     * @return array associative array
     */
    public function getApiConfig($storeId = null)
    {
        return array(
            'cache' => $this->isCacheEnabled($storeId),
            'cacheDir' => $this->directoryList->getPath('cache') . '/iy/',
            'endPoint' => $this->getApiEndpoint($storeId),
            'key' => $this->getApiKey($storeId),
            // name of the inboxify list you want to use as a default
            'list' => $this->getGeneralList(false, $storeId),
            // your API secret
            'secret' => $this->getApiSecret($storeId),
            // advanced: socket timeout
            'timeOutSocket' => 10,
            // advanced: socket stream read timeout
            'timeOutStream' => 10,
            'ttl' => $this->getCacheTtl($storeId),
        );
    }

    /**
     * Get API endpoint
     *
     * @param int|null $storeId
     * @return string
     */
    public function getApiEndpoint($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_API_ENDPOINT,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get API key
     *
     * @param int|null $storeId
     * @return string
     */
    public function getApiKey($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_API_KEY,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get API secret
     *
     * @param int|null $storeId
     * @return string
     */
    public function getApiSecret($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_API_SECRET,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get API list
     *
     * @param boolean $throwException throw exception if list not set
     * @param int|null $storeId
     * @return null|string null (if false = $throwException) or list string
     * @throws \Professio\Inboxify\Exception
     */
    public function getGeneralList($throwException = true, $storeId = null)
    {
        $generalList = $this->scopeConfig->getValue(
            self::CONFIG_PATH_GENERAL_LIST,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        if (empty($generalList)) {
            if ($throwException) {
                throw new \Exception(__('Inboxify list is not set.'));
            } else {
                $generalList = null;
            }
        }

        return $generalList;
    }

    /**
     * Get cache enabled
     *
     * @param int|null $storeId
     * @return integer
     */
    public function isCacheEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_CACHE_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get cache ttl
     *
     * @param int|null $storeId
     * @return integer
     */
    public function getCacheTtl($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PATH_CACHE_TTL,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
