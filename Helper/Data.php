<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Helper;

/**
 * Data helper
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Professio\Inboxify\Helper\Config
     */
    protected $configHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Sales\Model\Order\Config
     */
    protected $salesOrderConfig;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $salesResourceModelOrderCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Professio\Inboxify\Helper\Config $configHelper,
        \Magento\Sales\Model\Order\Config $salesOrderConfig,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesResourceModelOrderCollectionFactory
    ) {
        $this->configHelper = $configHelper;
        $this->logger = $context->getLogger();
        $this->salesOrderConfig = $salesOrderConfig;
        $this->salesResourceModelOrderCollectionFactory = $salesResourceModelOrderCollectionFactory;
        parent::__construct(
            $context
        );
    }

    /**
     * Test http headers signature
     *
     * @param array $headers
     * @return boolean
     */
    public function checkSignature($headers)
    {
        $signature = hash_hmac(
            'sha256',
            $headers['SALT'],
            $this->configHelper->getApiSecret(),
            true
        );
        
        $signatureEncoded = base64_encode($signature);
        
        $this->logger->debug(
            'inboxify/data_helper::checkSignature() '
            . 'headers: ' . json_encode($headers)
            . 'signature: ' . $signature
            . 'signature encoded: '
            . str_replace('%3d', '=', $signatureEncoded)
        );
        
        return $signatureEncoded
            == str_replace('%3d', '=', $signatureEncoded);
    }
    
    /**
     * Get order tags (ordered products category names)
     *
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    public function getOrderTags(\Magento\Sales\Model\Order $order)
    {
        $items = $order->getAllItems();
        $orderTags = array();

        foreach ($items as $item) {
            $product = $item->getProduct();
            $categoryCollection = $product->getCategoryCollection();
            $categoryCollection->addAttributeToSelect('name');
            $categoryCollection->clear()->load();

            foreach ($categoryCollection->getIterator() as $category) {
                $orderTags[] = $category->getName();
            }
        }
        
        return $orderTags;
    }
    
    /**
     * Retrieve customer's primary address of configured type
     * @param \Magento\Customer\Model\Customer $customer
     * @return \Magento\Customer\Model\Address
     */
    public function getCustomersPrimaryAddress(
        \Magento\Customer\Model\Customer $customer
    ) {
    
        $addressType = $this->configHelper
            ->getAdvancedAddressType();
        
        $this->logger->debug(
            'inboxify/data_helper::getCustomersPrimaryAddress() '
            . 'type: ' . $addressType
        );
        
        switch ($addressType) {
            case 'billing':
                $address = $customer->getPrimaryBillingAddress();
                break;
            case 'shipping':
                $address = $customer->getPrimaryShippingAddress();
                break;
        }
        
        $this->logger->debug(
            'inboxify/data_helper::getCustomersPrimaryAddress() '
            . ' address id: ' . ( $address ? $address->getEntityId() : 'no' )
        );
        
        return $address;
    }
    
    public function getCategoryNamesOfOrderedProducts(
        \Magento\Customer\Model\Customer $customer
    ) {
    
        $states = $this->salesOrderConfig
            ->getVisibleOnFrontStatuses();
        
        $collection = $this->salesResourceModelOrderCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customer->getId())
            ->addFieldToFilter('state', array('in' => $states))
            ->setOrder('created_at', 'desc');
        
        $collection->load();
        
        $tags = array();
        
        foreach ($collection->getIterator() as $order) {
            $tags = array_merge($tags, $this->getOrderTags($order));
        }
        
        return array_unique($tags);
    }
}
