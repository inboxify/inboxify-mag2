<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Controller\Manage;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Professio\Inboxify\Model\Client;
use Professio\Inboxify\Helper\Mapper;
use Psr\Log\LoggerInterface;

/**
 * Subscription management controller
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class Save extends Action
{
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var Client
     */
    protected $client;
    
    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var Mapper
     */
    protected $mapper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     * @param Context $context
     * @param Session $customerSession
     * @param Client $client
     * @param Mapper $mapper
     * @param LoggerInterface $logger
     * @param Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Client $client,
        Mapper $mapper,
        LoggerInterface $logger,
        Validator $formKeyValidator
    ) {
        parent::__construct($context);
        
        $this->customerSession = $customerSession;
        $this->client = $client;
        $this->mapper = $mapper;
        $this->formKeyValidator = $formKeyValidator;
        $this->logger = $logger;
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        if (!$this->customerSession->authenticate()) {
            $this->_actionFlag->set('', 'no-dispatch', true);
        }
        
        return parent::dispatch($request);
    }
    
    /**
     * Get current customer
     * @return Customer
     */
    protected function getCustomer()
    {
        return $this->getCustomerSession()->getCustomer();
    }
    
    /**
     * Get current customer session
     * @return Session
     */
    protected function getCustomerSession()
    {
        return $this->customerSession;
    }
    
    /**
     * Save single subscription
     * @return null
     */
    public function execute()
    {
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            return $this->_redirect('customer/account/');
        }

        try {
            $subscribe = $this->getRequest()
                ->getParam('inboxify_subscribe', false);
            
            $client = $this->client->getClient();
            $contact = $client->getContact($this->getCustomer()->getEmail());
            
            if (!$contact) {
                $contact = new \stdClass();
                
                $this->mapper->customerToContact(
                    $this->getCustomer(),
                    $contact
                );
                $new = true;
            } else {
                $new = false;
            }
            
            $contact->subscribe = $subscribe;
            $contact->unsubscribed = !$subscribe;
            
            if ($new) {
                $client->postContact($contact);
            } else {
                $client->putContact($contact->email, $contact);
            }
            
            $this->messageManager->addSuccessMessage(
                $subscribe ?
                __('The subscription has been saved.') :
                __('The subscription has been removed.')
            );
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('An error occurred while saving your subscription.')
            );
            
            $this->logger->critical($e);
        }
        
        $this->_redirect('customer/account/');
    }
}
