<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Controller\Manage;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RequestInterface;
use Psr\Log\LoggerInterface;

/**
 * Subscription management controller
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class Index extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     * @param Context $context
     * @param Session $customerSession
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        
        $this->customerSession = $customerSession;
        $this->logger = $logger;
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        if (!$this->customerSession->authenticate()) {
            $this->_actionFlag->set('', 'no-dispatch', true);
        }
        return parent::dispatch($request);
    }
    
    /**
     * Get current customer session
     *
     * @return Session
     */
    protected function getCustomerSession()
    {
        return $this->customerSession;
    }
    
    /**
     * Display subscription status
     *
     * @return null
     */
    public function execute()
    {
        $this->_view->loadLayout();

        if ($block = $this->_view->getLayout()->getBlock('inboxify_newsletter')) {
            $block->setRefererUrl($this->_redirect->getRefererUrl());
        }
        
        $this->_view->getPage()->getConfig()
            ->getTitle()->set(__('Newsletter Subscription'));
        
        $this->_view->renderLayout();
    }
}
