<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Controller\Subscriber;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Professio\Inboxify\Helper\Mapper;
use Professio\Inboxify\Model\Client;
use Psr\Log\LoggerInterface;

/**
 * Implementation of front-end subscriber
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class NewAction extends Action
{
    /**
     * @var \Professio\Inboxify\Model\Client
     */
    protected $client;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var Mapper
     */
    protected $mapper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     * @param Client $client
     * @param Context $context
     * @param Session $customerSession
     * @param LoggerInterface $logger
     * @param Mapper $mapper
     */
    public function __construct(
        Client $client,
        Context $context,
        Session $customerSession,
        LoggerInterface $logger,
        Mapper $mapper
    ) {
        parent::__construct($context);
        
        $this->client = $client;
        $this->customerSession = $customerSession;
        $this->logger = $logger;
        $this->mapper = $mapper;
    }

    /**
     * Get current customer
     *
     * @return Customer
     */
    protected function getCustomer()
    {
        return $this->getCustomerSession()->getCustomer();
    }
    
    /**
     * Get current customer session
     *
     * @return Session
     */
    protected function getCustomerSession()
    {
        return $this->customerSession;
    }
    
    /**
     * Get email input from request
     *
     * @return string
     */
    protected function getEmail()
    {
        return (string)$this->getRequest()->getPost('email');
    }
    
    /**
     * Get mapper
     *
     * @return Mapper
     */
    protected function getMapper()
    {
        return $this->mapper;
    }
    
    /**
     * Handle single subscription
     * INFO this doesn't allow to have single customer with multiple emails
     *
     * @return null
     */
    protected function subscribe()
    {
        $client = $this->client->getClient();
        $email = $this->getEmail();
        $subscribe = true;
        
        $contact = $client->getContact($email);
        
        if (!$contact) {
            $contact = new \stdClass();
            $new = true;
        } else {
            $new = false;
        }

        if ($this->getCustomer() && $this->getCustomer()->getId()) {
            $this->mapper->customerToContact(
                $this->getCustomer(),
                $contact
            );
        } else {
            $contact->email = $email;
        }

        $contact->subscribe = $subscribe;
        $contact->unsubscribed = !$subscribe;

        if ($new) {
            $client->postContact($contact);
        } else {
            $client->putContact($contact->email, $contact);
        }
    }
    
    /**
     * Single subscription action
     *
     * @return null
     */
    public function execute()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            try {
                $this->subscribe();
                $this->messageManager->addSuccessMessage(
                    __('The subscription has been saved.')
                );
            } catch (\Exception $e) {
                $this->logger->critical($e);
                
                $this->messageManager->addErrorMessage(
                    __('An error occurred while saving your subscription.')
                );
            }
        }
        
        $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
    }
}
