<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Controller\Adminhtml\Export;

use Magento\Backend\App\Action;

/**
 * Back-end controller for customer and newsletter subscriber mass actions
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class Index extends Action
{
    const RESOURCE_EXPORT = 'Professio_Inboxify::export';
    
    /**
     * Check if access to this action is allowed.
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(static::RESOURCE_EXPORT);
    }
    
    /**
     * Display export actions
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()
            ->set(__('Inboxify'));
        $this->_view->renderLayout();
    }
}
