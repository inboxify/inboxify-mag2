<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Controller\Adminhtml\Export;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Professio\Inboxify\Model\Exporter;
use Psr\Log\LoggerInterface;

/**
 * Back-end controller for customer and newsletter subscriber mass actions
 *
 * @category    Professio
 * @package     Professio\Inboxify
 */
class Customers extends Action
{
    /**
     * @var Exporter
     */
    protected $exporter;

    /**
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     * Constructor
     * @param Context $context
     * @param Exporter $exporter
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Exporter $exporter,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        
        $this->exporter = $exporter;
        $this->logger = $logger;
    }
    
    /**
     * Check if access to this action is allowed.
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(Index::RESOURCE_EXPORT);
    }
    
    /**
     * Get exporter model
     * @return Exporter
     */
    protected function getExporter()
    {
        return $this->exporter;
    }
    
    /**
     * Export customers action
     */
    public function execute()
    {
        try {
            $rs = $this->getExporter()->exportCustomers();
            
            $this->messageManager->addSuccessMessage(
                sprintf(
                    __(
                        'Customers export finished (completed: %d, fail: %d, '
                        . 'success: %d).'
                    ),
                    $rs['total'],
                    $rs['fail'],
                    $rs['success']
                )
            );
        } catch (\Exception $e) {
            $this->messageManager
                ->addErrorMessage(__('Customers export failed.'));
            $this->logger->critical($e);
        }
        
        $this->_redirect('inboxify/export/index');
    }
}
