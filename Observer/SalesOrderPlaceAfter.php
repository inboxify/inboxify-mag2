<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Observer;

use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Model\QuoteRepository;
use Professio\Inboxify\Helper\Config;
use Professio\Inboxify\Helper\Data;
use Professio\Inboxify\Helper\Mapper;
use Professio\Inboxify\Model\Client;
use Psr\Log\LoggerInterface;

/**
 * Handle sales order place after event: tag contact.
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class SalesOrderPlaceAfter implements ObserverInterface
{
    
    /**
     * @var Client
     */
    protected $client;
    
    /**
     * @var Config
     */
    protected $configHelper;
    
    /**
     * @var CustomerRegistry
     */
    protected $customerRegistry;
    
    /**
     * @var Data
     */
    protected $helper;
    
    /**
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     * @var ManagerInterface
     */
    protected $messageManager;
    
    /**
     * @var Mapper
     */
    protected $mapper;
    
    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;
    
    /**
     * Constructor
     * @param Config $configHelper
     * @param Data $helper
     * @param Client $client
     * @param LoggerInterface $logger
     */
    public function __construct(
        Config $configHelper,
        Data $helper,
        Client $client,
        LoggerInterface $logger,
        ManagerInterface $messageManager,
        Mapper $mapper,
        CustomerRegistry $customerRegistry,
        QuoteRepository $quoteRepository
    ) {
    
        $this->configHelper = $configHelper;
        $this->client = $client;
        $this->customerRegistry = $customerRegistry;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->mapper = $mapper;
        $this->messageManager = $messageManager;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * Handle sales order place after event: tag contact.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' start');
        
        try {
            if (!$this->configHelper
                ->isAdvancedOnOrderEnabled()
            ) {
                // nothing to do
                return;
            }

            $client = $this->client->getClient();
            $order = $observer->getEvent()->getOrder();
            $email = $order->getCustomerEmail();
            $contact = $client->getContact($email);

            try {
                $quote = $this->quoteRepository->get($order->getQuoteId());
                $subscribe = $quote->getNewsletterSubscribe();
                
                if (!$contact && $subscribe) {
                    $contact = new \stdClass();
                    $contact->subscribe = true;
                    $contact->unsubscribed = false;
                    $new = true;
                } else {
                    $new = false;
                }
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
            
            if (!$contact && !$subscribe) {
                // nothing to do 2
                return;
            }
            
            if ($order->getCustomerId()) {
                $customer = $this->customerRegistry->retrieve(
                    $order->getCustomerId()
                );
                
                $this->mapper->customerToContact($customer, $contact);
            } else {
                $this->mapper->orderToContact($order, $contact);
                
                if ($this->configHelper->isAddressTypeBilling()) {
                    $this->mapper->addressToContact(
                        $order->getBillingAddress(),
                        $contact
                    );
                } else {
                    $this->mapper->addressToContact(
                        $order->getShippingAddress(),
                        $contact
                    );
                }
            }
            
            if (!isset($contact->tags) || !is_array($contact->tags)) {
                $contact->tags = array();
            }
            
            $contact->tags = array_merge(
                $contact->tags,
                $this->helper->getOrderTags($order)
            );

            if ($new) {
                $client->postContact($contact);
            } else {
                $client->putContact($email, $contact);
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' end');
    }
}
