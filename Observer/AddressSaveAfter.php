<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Professio\Inboxify\Model\Client;
use Professio\Inboxify\Helper\Config;
use Professio\Inboxify\Helper\Mapper;
use Psr\Log\LoggerInterface;

/**
 * Handle address save after event: map address to contact.
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class AddressSaveAfter implements ObserverInterface
{
    /**
     * @var Client
     */
    protected $client;
    
    /**
     * @var Config
     */
    protected $configHelper;
    
    /**
     * @var Mapper
     */
    protected $mapper;
    
    /**
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     * Constructor
     * @param Client $client
     * @param Config $configHelper
     * @param Mapper $mapper
     * @param LoggerInterface $logger
     */
    public function __construct(
        Client $client,
        Config $configHelper,
        Mapper $mapper,
        LoggerInterface $logger
    ) {
    
        $this->client = $client;
        $this->mapper = $mapper;
        $this->configHelper = $configHelper;
        $this->logger = $logger;
    }

    /**
     * Check if current address is the right type.
     * @param type $address
     * @param type $customer
     */
    protected function isAddressSaveAfter($address, $customer)
    {
        return (
            $this->configHelper->isAddressTypeBilling()
            && $address->getIsDefaultBilling()
        ) || (
            $this->configHelper->isAddressTypeShipping()
            && $address->getIsDefaultShipping()
        );
    }
    
    /**
     * Handle address save after event: map address to contact.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' start');
        
        try {
            if ($this->configHelper
                    ->isAdvancedOnAddressUpdateEnabled()
                ) {
                $address = $observer->getCustomerAddress();
                $customer = $address->getCustomer();
                
                if ($this->isAddressSaveAfter($address, $customer)) {
                    if ($customer && $customer->getEntityId()) {
                        $client = $this->client->getClient();
                        $contact = $client->getContact($customer->getEmail());

                        if ($contact) {
                            $this->mapper->addressToContact(
                                $address,
                                $contact
                            );

                            $client->putContact($contact->email, $contact);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' end');
    }
}
