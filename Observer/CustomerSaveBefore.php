<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Observer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Handle customer save after front event: map data to contact.
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class CustomerSaveBefore implements ObserverInterface
{
    protected $customerRepository;
    
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }
    
    public function execute(Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        
        if ($customer->getId()) {
            $customerOld = $this->customerRepository
                ->getById($customer->getId());

            // INFO: because customer orig data in save after handlers is empty
            // i have to use this work-around to get customer email before
            // the customer account and possibly also email (unique id in 
            // newsletter) is changed
            $GLOBALS['inboxify_email_' . $customer->getId()] =
                $customerOld->getEmail();
        }
    }
}
