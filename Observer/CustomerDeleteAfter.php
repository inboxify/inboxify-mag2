<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Professio\Inboxify\Model\Client;
use Professio\Inboxify\Helper\Config;
use Psr\Log\LoggerInterface;

/**
 * Handle customer delete after event: delete / unsubscribe contact.
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class CustomerDeleteAfter implements ObserverInterface
{
    /**
     * @var Client
     */
    protected $client;
    
    /**
     * @var Config
     */
    protected $configHelper;
    
    /**
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     * @var ManagerInterface
     */
    protected $messageManager;
    
    /**
     * Constructor
     * @param Client $client
     * @param Config $configHelper
     * @param LoggerInterface $logger
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Client $client,
        Config $configHelper,
        LoggerInterface $logger,
        ManagerInterface $messageManager
    ) {
    
        $this->client = $client;
        $this->configHelper = $configHelper;
        $this->logger = $logger;
        $this->messageManager = $messageManager;
    }
    
    /**
     * Handle customer delete after event: delete / unsubscribe contact.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' start');
        
        try {
            if ($this->configHelper
                    ->isAdvancedOnCustomerDeleteEnabled()
                || $this->configHelper
                    ->isAdvancedOnCustomerDeleteUnsubscribeEnabled()
                ) {
                $customer = $observer->getEvent()->getCustomer();
                $client = $this->client
                    ->getStoreClient($customer->getData('store_id'));
                $email = $customer->getEmail();
                $contact = $client->getContact($email);
               
                if ($contact) {
                    if ($this->configHelper
                        ->isAdvancedOnCustomerDeleteUnsubscribeEnabled()) {
                        $contact->subscribe = false;
                        $contact->unsubscribed = true;

                        $client->putContact($contact->email, $contact);
                       
                        $this->logger->debug(
                            __CLASS__ . '::' . __METHOD__
                            . ' unsubscribe / delete contact for customer id: '
                            . $customer->getEntityId()
                        );
                    }
                    
                    $client->deleteContact($email);
                    
                    $this->logger->debug(
                        __CLASS__ . '::' . __METHOD__
                        . ' deleted contact for customer id: '
                        . $customer->getEntityId()
                    );
                } else {
                    $this->logger->debug(
                        __CLASS__ . '::' . __METHOD__
                        . ' contact not found for customer id: '
                        . $customer->getEntityId()
                    );
                }
            } else {
                $this->logger->debug(
                    __CLASS__ . '::' . __METHOD__ . ' disabled'
                );
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' end');
    }
}
