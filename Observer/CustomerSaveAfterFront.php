<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use \Magento\Framework\App\Request\Http;
use Professio\Inboxify\Model\Client;
use Professio\Inboxify\Helper\Config;
use Professio\Inboxify\Helper\Mapper;
use Psr\Log\LoggerInterface;

/**
 * Handle customer save after front event: map data to contact.
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class CustomerSaveAfterFront implements ObserverInterface
{
    /**
     * @var Client
     */
    protected $client;
    
    /**
     * @var Config
     */
    protected $configHelper;
    
    /**
     * @var Logger
     */
    protected $logger;
    
    /**
     * @var Mapper
     */
    protected $mapper;
    
    /**
     * @var ManagerInterface
     */
    protected $messageManager;
    
    /**
     * @var Http
     */
    protected $request;
    
    /**
     * Constructor
     * @param Client $client
     * @param Config $configHelper
     * @param Mapper $mapper
     * @param LoggerInterface $logger
     * @param ManagerInterface $messageManager
     * @param Http $request
     */
    public function __construct(
        Client $client,
        Config $configHelper,
        Mapper $mapper,
        LoggerInterface $logger,
        ManagerInterface $messageManager,
        Http $request
    ) {
    
        $this->client = $client;
        $this->configHelper = $configHelper;
        $this->mapper = $mapper;
        $this->logger = $logger;
        $this->messageManager = $messageManager;
        $this->request = $request;
    }

    /**
     * Handle customer save after front event: map data to contact.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' start');
        
        try {
            if (!$this->configHelper
                ->isAdvancedOnCustomerUpdateEnabled() ) {
                // nothing to do
                return;
            }
            
            $client = $this->client->getClient();
            $customer = $observer->getEvent()->getCustomer();
            
            // INFO: because customer orig data in save after handlers is empty
            // i have to use this work-around to get customer email before
            // the customer account and possibly also email (unique id in 
            // newsletter) is changed
            $email = isset($GLOBALS['inboxify_email_' . $customer->getId()])
                ? $GLOBALS['inboxify_email_' . $customer->getId()]
                : $customer->getEmail();
            
            $contact = $client->getContact($email);
            $subscribe = $this->request->getPost('iy_is_subscribed');
            
            if (!$contact && !$subscribe) {
                // nothing to do 2
                return;
            }
            
            if (!$contact) {
                $contact = new \stdClass();
                $new = true;
            } else {
                $new = false;
            }

            $this->mapper->customerToContact($customer, $contact);

            $contact->subscribe = $subscribe;
            $contact->unsubscribed = !$subscribe;

            if ($new) {
                $client->postContact($contact);
            } else {
                $client->putContact($email, $contact);
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' end');
    }
}
