<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Professio\Inboxify\Helper\Config;
use Professio\Inboxify\Model\Client;
use Psr\Log\LoggerInterface;

/**
 * Handle after config change: load available lists.
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class AfterConfigChange implements ObserverInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * Constructor
     * @param Client $client
     * @param ManagerInterface $messageManager
     * @param Config $configHelper
     * @param LoggerInterface $logger
     */
    public function __construct(
        Client $client,
        ManagerInterface $messageManager,
        Config $configHelper,
        LoggerInterface $logger
    ) {
        $this->client = $client;
        $this->configHelper = $configHelper;
        $this->logger = $logger;
        $this->messageManager = $messageManager;
    }
    
    /**
     * Handle after config change: load available lists.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' start');
        
        try {
            if (!$this->client->getClient()->isConnected()) {
                $this->messageManager->addErrorMessage(
                    __('Invalid API endpoint, key and secret combination.')
                );
            } else {
                $this->messageManager->addSuccessMessage(
                    __(
                        'API endpoint, key and secret combination is valid.'
                    )
                );
            }
            
            if (!$this->configHelper->isCacheEnabled()) {
                $this->client->getClient()->getCache()->purge();
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->logger->critical($e);
        }
        
        $this->logger->debug(__CLASS__ . '::' . __METHOD__ . ' end');
    }
}
