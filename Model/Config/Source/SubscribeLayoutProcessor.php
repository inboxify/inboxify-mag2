<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Model\Config\Source;

use Professio\Inboxify\Helper\Config;
use Professio\Inboxify\Model\Config\Source\Account;

/**
 * SubscribeLayoutProcessor - update onepage checkout layout.
 */
class SubscribeLayoutProcessor
{
    /**
     * @var Config
     */
    protected $_configHelper;

    /**
     * @param Config $helper
     */
    public function __construct(Config $configHelper)
    {
        $this->_configHelper = $configHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function process($jsLayout)
    {
        switch ($this->_configHelper->getAdvancedCreateAccount()) {
            case Account::CHECKED:
                $checked = true;
                $changeable = true;
                $visible = true;
                break;
            case Account::UNCHECKED:
                $checked = false;
                $changeable = true;
                $visible = true;
                break;
            case Account::HIDDENCHECKED:
                $checked = true;
                $changeable = false;
                $visible = false;
                break;
        }
        
        $jsLayoutSubscribe = [
            'components' => [
                'checkout' => [
                    'children' => [
                        'steps' => [
                            'children' => [
                                'billing-step' => [
                                    'children' => [
                                        'payment' => [
                                            'children' => [
                                                'customer-email' => [
                                                    'children' => [
                                                        'newsletter-subscribe' => [
                                                            'config' => [
                                                                'checkoutLabel' => __('Sign Up for Newsletter'),
                                                                'checked' => $checked,
                                                                'visible' => $visible,
                                                                'changeable' => $changeable
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                'shipping-step' => [
                                    'children' => [
                                        'shippingAddress' => [
                                            'children' => [
                                                'customer-email' => [
                                                    'children' => [
                                                        'newsletter-subscribe' => [
                                                            'config' => [
                                                                'checkoutLabel' => __('Sign Up for Newsletter'),
                                                                'checked' => $checked,
                                                                'visible' => $visible,
                                                                'changeable' => $changeable
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return array_merge_recursive($jsLayout, $jsLayoutSubscribe);
    }
}
