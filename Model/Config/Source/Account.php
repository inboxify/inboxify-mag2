<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Model\Config\Source;

/**
 * Account type config source
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class Account
{
    const CHECKED = 'checked';
    const UNCHECKED = 'unchecked';
    const HIDDENCHECKED = 'hiddenchecked';
    
    /**
     * Options getter
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'label' => __('Display checked checkbox'),
                'value' => self::CHECKED
            ),
            array(
                'label' => __('Display unchecked checkbox'),
                'value' => self::UNCHECKED
            ),
            array(
                'label' => __('Don\'t display checkbox, sign-up automatically'),
                'value' => self::HIDDENCHECKED
            ),
        );
    }
}
