<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Model\Config\Source\Delete;

/**
 * Delete address config source
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class Address
{
    const ON_DELETE_USE_NEW = 'use_new';
    const ON_DELETE_IGNORE = 'ignore';
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'label' => __('Use new primary address'),
                'value' => self::ON_DELETE_USE_NEW
            ),
            array(
                'label' => __('Do nothing'),
                'value' => self::ON_DELETE_IGNORE
            ),
        );
    }
}
