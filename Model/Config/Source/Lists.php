<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Model\Config\Source;

use Professio\Inboxify\Model\Client;
use Psr\Log\LoggerInterface;

/**
 * Select list config source
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class Lists
{
    /**
     * hash of lists id => name
     * @var array
     */
    protected $lists;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     * @param Client $clientFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        Client $client,
        LoggerInterface $logger
    ) {
        $this->client = $client;
        $this->logger = $logger;
    }
    
    /**
     * Get all available lists
     * @return array
     */
    public function getLists()
    {
        if (!isset($this->lists)) {
            try {
                $lists = $this->client->getClient()->getLists();
                
                if (is_array($lists) && count($lists)) {
                    foreach ($lists as $list) {
                        $this->lists[$list->id] = $list->list;
                    }
                }
            } catch (\Exception $e) {
                $this->lists = array();
                $this->logger->critical($e);
            }
        }
        
        return $this->lists;
    }
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        static $options;
        
        if (!isset($options)) {
            $options = array();
            
            $options[] = array(
                'value' => '',
                'label' => __('Select List')
            );
            
            foreach ($this->getLists() as $listId => $listName) {
                $options[] = array(
                    'value' => $listId,
                    'label' => $listName
                );
            }
        }
        
        return $options;
    }
}
