<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Model\Config\Source;

/**
 * Delete entity config source
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class Delete
{
    const ON_DELETE_DELETE = 'delete';
    const ON_DELETE_DEL_UNSUB = 'deleteunsub';
    const ON_DELETE_IGNORE = 'ignore';
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'label' => __('Delete Contact'),
                'value' => self::ON_DELETE_DELETE
            ),
            array(
                'label' => __('Delete and unsubscribe Contact'),
                'value' => self::ON_DELETE_DEL_UNSUB
            ),
            array(
                'label' => __('Do nothing'),
                'value' => self::ON_DELETE_IGNORE
            ),
        );
    }
}
