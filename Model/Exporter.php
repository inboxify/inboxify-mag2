<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Model;

use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory as SubscriberCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Professio\Inboxify\Model\Client;
use Professio\Inboxify\Helper\Config;
use Professio\Inboxify\Helper\Data;
use Professio\Inboxify\Helper\Mapper;
use Psr\Log\LoggerInterface;

/**
 * Exporter model
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class Exporter
{
    /**
     * Debugging flag
     * @var boolean
     */
    protected $debug = true;
    
    /**
     * List of store ids
     * @var array
     */
    protected $storeIds;
    
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Mapper
     */
    protected $mapper;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CustomerCollectionFactory
     */
    protected $customerCollectionFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var SubscriberCollectionFactory
     */
    protected $subscriberCollectionFactory;

    /**
     * @var CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * Constructor
     * @param Client $client
     * @param StoreManagerInterface $storeManager
     * @param Mapper $mapper
     * @param Data $helper
     * @param CustomerCollectionFactory $customerCollectionFactory
     * @param LoggerInterface $logger
     * @param SubscriberCollectionFactory $subscriberCollectionFactory
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param Config $configHelper
     */
    public function __construct(
        Client $client,
        StoreManagerInterface $storeManager,
        Mapper $mapper,
        Data $helper,
        CustomerCollectionFactory $customerCollectionFactory,
        LoggerInterface $logger,
        SubscriberCollectionFactory $subscriberCollectionFactory,
        OrderCollectionFactory $orderCollectionFactory,
        Config $configHelper
    ) {
        $this->client = $client;
        $this->storeManager = $storeManager;
        $this->mapper = $mapper;
        $this->helper = $helper;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->logger = $logger;
        $this->subscriberCollectionFactory = $subscriberCollectionFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->configHelper = $configHelper;
    }
    
    /**
     * Get Inboxify API client
     *
     * @return \Inboxify\Api\Client
     */
    protected function getClient()
    {
        return $this->client->getClient();
    }
    
    /**
     * Get list of store ids
     * @return array
     */
    protected function getStoreIds()
    {
        if (!isset($this->storeIds)) {
            $stores = $this->storeManager->getStores();
            $this->storeIds = array();

            foreach ($stores as $store) {
                $this->storeIds[] = $store->getId();
            }
        }
        
        return $this->storeIds;
    }
    
    /**
     * Get mapper helper
     * @return \Professio\Inboxify\Helper\Mapper
     */
    protected function getMapper()
    {
        return $this->mapper;
    }
    
    /**
     * Get inboxify helper
     * @return \Professio\Inboxify\Helper\Data
     */
    protected function getHelper()
    {
        return $this->helper;
    }
    
    /**
     * Export all customers to Inboxify API
     *
     * @return array
     */
    public function exportCustomers()
    {
        $this->log('inboxify/exporter::exportCustomers() start');
        
        $storeIds = $this->getStoreIds();
        $totals = array('total' => 0, 'fail' => 0, 'success' => 0);
        
        foreach ($storeIds as $storeId) {
            $this->exportCustomersStore($storeId, $totals);
        }
        
        $this->log('inboxify/exporter::exportCustomers() end');
        
        return $totals;
    }
    
    /**
     * Export customers from store
     * @param integer $storeId
     * @param array $totals
     */
    public function exportCustomersStore($storeId, &$totals)
    {
        $this->log(
            'inboxify/exporter::exportCustomersStore() start (store id: '
            . $storeId . ').'
        );

        try {
            $client = $this->client
                ->getStoreClient($storeId);
            
            $collection = $this->customerCollectionFactory->create();
            $collection
                ->addAttributeToSelect('*')
                ->addFieldToFilter('store_id', $storeId)
                ->setPageSize(\Inboxify\Api\Client::LIMIT);

            $total = $collection->getSize();
            
            if ($total > 0) {
                $page = 1;
                $pages = ceil($total / \Inboxify\Api\Client::LIMIT);

                $this->log(
                    'inboxify/exporter::exportCustomersStore() customers: '
                    . $total . ', customer pages: ' . $pages
                );
                
                do {
                    $collection->clear();
                    $collection->setCurPage($page);
                    $collection->load();

                    $this->exportCustomersStorePage(
                        $client,
                        $collection,
                        $totals
                    );
                    
                    $page++;
                } while ($page <= $pages);
            } else {
                $this->log(
                    'inboxify/exporter::exportCustomersStore() no customers'
                );
            }
        } catch (\Exception $e) {
            $this->log(
                'inboxify/exporter::exportCustomersStore() failed '
                . 'with exception: ' . $e->getMessage()
            );
            
            $this->logger->critical($e);
        }
        
        $this->log('inboxify/exporter::exportCustomersStore() end');
    }
    
    /**
     * Export one page of customers from store
     * @param \Inboxify\Api\Client $client
     * @param \Magento\Customer\Model\ResourceModel\Customer\Collection $collection
     * @param array $totals
     */
    public function exportCustomersStorePage($client, $collection, &$totals)
    {
        $this->log('inboxify/exporter::exportCustomersStorePage() start');

        try {
            $contacts = array();

            foreach ($collection->getIterator() as $customer) {
                $contact = new \stdClass();
                $this->getMapper()->customerToContact($customer, $contact);
                
                $contact->unsubscribed = false;
                $contact->subscribe = true;
                
                $tags = $this->getHelper()
                    ->getCategoryNamesOfOrderedProducts($customer);
                
                if ($tags) {
                    $contact->tags = $tags;
                }
                
                $contacts[] = $contact;
            }

            if (count($contacts)) {
                list($total, $fail, $success) =
                    $client->postContactsBulk($contacts);
                
                $totals['total'] += $total;
                $totals['fail'] += $fail;
                $totals['success'] += $success;
            }
        } catch (\Exception $e) {
            $this->log(
                'inboxify/exporter::exportCustomersStorePage() failed '
                . 'with exception: ' . $e->getMessage()
            );
            
            $this->logger->critical($e);
        }
        
        $this->log('inboxify/exporter::exportCustomersStorePage() end');
    }
    
    /**
     * Export newsletter subscribers
     * @return array
     */
    public function exportSubscribers()
    {
        $this->log('inboxify/exporter::exportSubscribers() start');
        
        $storeIds = $this->getStoreIds();
        $totals = array('total' => 0, 'fail' => 0, 'success' => 0);
        
        foreach ($storeIds as $storeId) {
            $this->exportSubscribersStore($storeId, $totals);
        }
        
        $this->log('inboxify/exporter::exportSubscribers() end');
        
        return $totals;
    }
    
    /**
     * Export subscribers from one store
     * @param integer $storeId
     * @param array $totals
     */
    public function exportSubscribersStore($storeId, &$totals)
    {
        $this->log('inboxify/exporter::exportSubscribersStore() start');

        try {
            $client = $this->client
                ->getStoreClient($storeId);
            
            $collection = $this->subscriberCollectionFactory->create();
            $collection
                ->addFieldToFilter('store_id', $storeId)
                ->setPageSize(\Inboxify\Api\Client::LIMIT);

            $total = $collection->getSize();
            
            if ($total > 0) {
                $page = 1;
                $pages = ceil($total / \Inboxify\Api\Client::LIMIT);

                $this->log(
                    'inboxify/exporter::exportSubscribersStore() '
                    . 'subscribers: ' . $total . ', subscriber pages: ' . $pages
                );
                
                do {
                    $collection->clear();
                    $collection->setCurPage($page);
                    $collection->load();

                    $this->exportSubscribersStorePage(
                        $client,
                        $collection,
                        $totals
                    );
                    
                    $page++;
                } while ($page <= $pages);
            } else {
                $this->log(
                    'inboxify/exporter::exportSubscribersStore() no subscr.'
                );
            }
        } catch (\Exception $e) {
            $this->log(
                'inboxify/exporter::exportSubscribersStore() failed '
                . 'with exception: ' . $e->getMessage()
            );
            
            $this->logger->critical($e);
        }
        
        $this->log('inboxify/exporter::exportSubscribersStore() end');
    }
    
    /**
     * Export one page of subscribers from store
     * @param \Inboxify\Api\Client $client
     * @param \Magento\Newsletter\Model\ResourceModel\Subscriber\Collection $collection
     * @param array $totals
     */
    public function exportSubscribersStorePage($client, $collection, &$totals)
    {
        $this->log('inboxify/exporter::exportSubscribersStorePage() start');
        
        try {
            $contacts = array();

            foreach ($collection->getIterator() as $subscriber) {
                $contact = new \stdClass();
                $this->getMapper()->subscriberToContact($subscriber, $contact);
                
                $contact->unsubscribed = false;
                $contact->subscribe = true;
                
                $customer = $subscriber->getCustomer();
                
                if ($customer && $customer->getEntityId()) {
                    $this->getMapper()->customerToContact($customer, $contact);
                    
                    $tags = $this->getHelper()
                        ->getCategoryNamesOfOrderedProducts($customer);
                    
                    if ($tags) {
                        $contact->tags = $tags;
                    }
                }
                
                $contacts[] = $contact;
            }

            if (count($contacts)) {
                list($total, $fail, $success) =
                    $client->postContactsBulk($contacts);
                
                $totals['total'] += $total;
                $totals['fail'] += $fail;
                $totals['success'] += $success;
            }
        } catch (\Exception $e) {
            $this->log(
                'inboxify/exporter::exportCustomersStorePage() failed '
                . 'with exception: ' . $e->getMessage()
            );
            
            $this->logger->critical($e);
        }
        
        $this->log('inboxify/exporter::exportSubscribersStorePage() end');
    }
    
    /**
     * Export unregistered customers
     * @return array
     */
    public function exportUnregistered()
    {
        $this->log('inboxify/exporter::exportUnregistered() start');
        
        $storeIds = $this->getStoreIds();
        $totals = array('total' => 0, 'fail' => 0, 'success' => 0);
        
        foreach ($storeIds as $storeId) {
            $this->exportUnregisteredStore($storeId, $totals);
        }
        
        $this->log('inboxify/exporter::exportUnregistered() end');
        
        return $totals;
    }
    
    /**
     * Export unregistered customers from one store
     * @param integer $storeId
     * @param array $totals
     */
    public function exportUnregisteredStore($storeId, &$totals)
    {
        $this->log('inboxify/exporter::exportUnregisteredStore() start');

        try {
            $client = $this->client
                ->getStoreClient($storeId);

            $collection = $this->orderCollectionFactory->create();
            $collection
                ->addAttributeToFilter('customer_id', array('null' => true))
                ->addFieldToFilter('store_id', $storeId)
                ->setPageSize(\Inboxify\Api\Client::LIMIT);
            
            // avoid duplicates
            $collection->getSelect()->group('main_table.customer_email');

            $total = $collection->getSize();
            
            if ($total > 0) {
                $page = 1;
                $pages = ceil($total / \Inboxify\Api\Client::LIMIT);

                $this->log(
                    'inboxify/exporter::exportUnregisteredStore() '
                    . 'orders: ' . $total . ', order pages: ' . $pages
                );
                
                do {
                    $collection->clear();
                    $collection->setCurPage($page);
                    $collection->load();

                    $this->exportUnregisteredStorePage(
                        $client,
                        $collection,
                        $totals
                    );
                    
                    $page++;
                } while ($page <= $pages);
            } else {
                $this->log(
                    'inboxify/exporter::exportUnregisteredStore() no orders'
                );
            }
        } catch (\Exception $e) {
            $this->log(
                'inboxify/exporter::exportUnregisteredStore() failed '
                . 'with exception: ' . $e->getMessage()
            );
            
            $this->logger->critical($e);
        }
        
        $this->log('inboxify/exporter::exportUnregisteredStore() end');
    }
    
    /**
     * Export one page of unregistered customers from one store
     * @param \Inboxify\Api\Client $client
     * @param Magento\Sales\Model\ResourceModel\Order\Collection $collection
     * @param array $totals
     */
    public function exportUnregisteredStorePage($client, $collection, &$totals)
    {
        $this->log(
            'inboxify/exporter::exportUnregisteredStorePage() start'
        );
        
        try {
            $contacts = array();

            foreach ($collection->getIterator() as $order) {
                $contact = new \stdClass();
                $this->getMapper()->orderToContact($order, $contact);
                
                $contact->unsubscribed = false;
                $contact->subscribe = true;
                
                $tags = $this->getHelper()->getOrderTags($order);

                if ($tags) {
                    $contact->tags = $tags;
                }
                
                $contacts[] = $contact;
            }

            if (count($contacts)) {
                list($total, $fail, $success) =
                    $client->postContactsBulk($contacts);
                
                $totals['total'] += $total;
                $totals['fail'] += $fail;
                $totals['success'] += $success;
            }
        } catch (\Exception $e) {
            $this->log(
                'inboxify/exporter::exportUnregisteredStorePage() failed '
                . 'with exception: ' . $e->getMessage()
            );
            
            $this->logger->critical($e);
        }
        
        $this->log('inboxify/exporter::exportSubscribersStorePage() end');
    }
    
    /**
     * Custom log wrapper - log only in developer mode
     *
     * @param string $message
     */
    protected function log($message)
    {
        if ($this->debug) {
            $this->logger->debug($message);
        }
    }
}
