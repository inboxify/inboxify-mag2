<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Model\Plugin\Checkout;

use Magento\Quote\Model\QuoteRepository;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement as ModelShippingInformationManagement;
use Professio\Inboxify\Helper\Config;
use Professio\Inboxify\Model\Config\Source\Account;

/**
 * ShippingInformationManagement - store newsletter subscribe.
 */
class ShippingInformationManagement
{
    /**
     * @var Config
     */
    protected $_configHelper;

    /**
     * @var QuoteRepository
     */
    protected $_quoteRepository;

    /**
     * @param QuoteRepository $quoteRepository
     * @param Config $configHelper
     */
    public function __construct(
        QuoteRepository $quoteRepository,
        Config $configHelper
    ) {
        $this->_quoteRepository = $quoteRepository;
        $this->_configHelper = $configHelper;
    }

    /**
     * @param ModelShippingInformationManagement $subject
     * @param int $cartId
     * @param ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        ModelShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        if (Account::HIDDENCHECKED == $this->_configHelper->getAdvancedCreateAccount()) {
            $newsletterSubscribe = 1;
        } else {
            $newsletterSubscribe = $addressInformation->getExtensionAttributes()
                ->getNewsletterSubscribe() ? 1 : 0;
        }

        $quote = $this->_quoteRepository->getActive($cartId);
        $quote->setNewsletterSubscribe($newsletterSubscribe);
    }
}
