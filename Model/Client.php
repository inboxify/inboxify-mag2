<?php
/**
 * Professio\Inboxify extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 *
 * @category       Professio
 * @package        Professio\Inboxify
 * @copyright      Copyright (c) 2017
 * @license        https://gitlab.com/inboxify/inboxify-mag2/blob/master/LICENSE
 */

namespace Professio\Inboxify\Model;

use Magento\Store\Model\App\Emulation;
use Professio\Inboxify\Helper\Config;

/**
 * Client model
 *
 * @category   Professio
 * @package    Professio\Inboxify
 */
class Client
{
    /**
     * @var \Inboxify\Api\Client
     */
    protected $client;
    
    /**
     * Hash of store clients
     * @var array
     */
    protected $storeClients = array();

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var Emulation
     */
    protected $storeAppEmulation;

    /**
     * Constructor
     * @param Config $configHelper
     * @param Emulation $storeAppEmulation
     */
    public function __construct(
        Config $configHelper,
        Emulation $storeAppEmulation
    ) {
    
        $this->configHelper = $configHelper;
        $this->storeAppEmulation = $storeAppEmulation;

        $this->init();
    }
    
    /**
     * Initiate client
     */
    protected function init()
    {
        $config = new \Inboxify\Api\Config(
            $this->configHelper->getApiConfig()
        );
        $cache = new \Inboxify\Api\Cache($config);
        
        $this->client = new \Inboxify\Api\Client($cache, $config);
    }
    
    /**
     * Get API client
     * @return \Inboxify\Api\Client
     */
    public function getClient()
    {
        return $this->client;
    }
    
    /**
     * Get API client for store by store id
     * @param integer $storeId
     * @return \Inboxify\Api\Client
     */
    public function getStoreClient($storeId)
    {
        if (!isset($this->storeClients[$storeId])) {
            $config = new \Inboxify\Api\Config(
                $this->configHelper->getApiConfig($storeId)
            );
            $cache = new \Inboxify\Api\Cache($config);
            $this->storeClients[$storeId] = new \Inboxify\Api\Client(
                $cache,
                $config
            );
        }
        
        return $this->storeClients[$storeId];
    }
}
